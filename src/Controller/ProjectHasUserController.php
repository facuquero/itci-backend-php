<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\jwtAuth;
use App\Service\Helpers;
use App\Entity\Project;
use App\Entity\ProjectHasUser;
use App\Entity\User;

class ProjectHasUserController extends AbstractController {

    /**
     * @Route("/api/project_has_user", name="add_project_has_user", methods={"POST", "OPTIONS"} )
     */
    public function add(Request $request, Helpers $helpers, $params = [], jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);
        if (!$token['check']) {
            return new JsonResponse(['message' => 'You are not authorized', 'ok' => false], 403);
        }
        $parameters = $request->request->all();
        if (!empty($params)) {
            $parameters = $params;
        }
        $user = $this->getDoctrine()->getManager()->getRepository(User::class)->findOneBy(['id' => $parameters['user']]);
        $project = $this->getDoctrine()->getManager()->getRepository(Project::class)->findOneBy(['id' => $parameters['project']]);

        if ($user) {
            if ($project) {
                $projectHasUser = new ProjectHasUser();
                $projectHasUser->setUser($user);
                $projectHasUser->setProject($project);
                $errors = $helpers->validateEntity($projectHasUser);
                if (!$errors) {
                    $projectHasUser = $helpers->saveEntity($projectHasUser);
                    return new JsonResponse([
                        'ok' => true,
                        'data' => $projectHasUser->getId(),
                        'message' => 'Success, user added to project!'
                            ], 201);
                }
                return new JsonResponse([
                    'ok' => false,
                    'message' => 'projectHasUser exist'
                        ], 403);
            }
            return new JsonResponse([
                'ok' => false,
                'message' => 'project not found'
                    ], 404);
        }
        return new JsonResponse([
            'ok' => false,
            'message' => 'User not found'
                ], 404);
    }

    /**
     * @Route("/api/project_has_user/{projectId}/{userId}", name="delete_project_has_user", methods={"DELETE", "OPTIONS"} )
     */
    public function deleteProjectHasUser(Helpers $helpers, $projectId, $userId, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);
        if ($token['check'] && $user->getRoles()[0] == 'ROLE_ADMIN') {
                $projectHasUserDelete = $this->getDoctrine()->getRepository(ProjectHasUser::class)->findOneBy(['project' => $projectId, 'user' => $userId]);
                if ($projectHasUserDelete) {
                    $helpers->removeEntity($projectHasUserDelete);
                    return new JsonResponse(['message' => 'User deleted from project', 'ok' => true], 200);
                }
                return new JsonResponse(['message' => 'User or project not found', 'ok' => false], 400);
        }
        return new JsonResponse(['message' => 'You are not authorized', 'ok' => false], 400);
    }
   

    /**
     * @Route("/api/activate_project/{projectId}/{userId}", name="activate_project", methods={"PUT", "OPTIONS"} )
     */
    public function activateProject(Request $request, Helpers $helpers, jwtAuth $jwtAuth, $projectId, $userId) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);
        if ($token['check']  && $user->getRoles()[0] == 'ROLE_ADMIN') {
            $parameters = $request->request->all();
            $aProject = $this->getDoctrine()->getRepository(ProjectHasUser::class)->findOneBy(['project' => $projectId, 'user' => $userId]);
            if ($aProject) {
                if (!$aProject->getActive(0)) {
                    $aProject->setActive(1);
                    $aProject = $helpers->saveEntity($aProject);
                    return new JsonResponse(['message' => 'Success, user activated in project!', 'ok' => true], 200);
                }
                return new JsonResponse(['message' => 'User is already active in this project', 'ok' => false], 400);
            }
            return new JsonResponse(['message' => 'User orProject not exist', 'errors', 'ok' => false], 400);
        }
        return new JsonResponse(['message' => 'Not authorized', 'ok' => false], 404);
    }

         /**
     * @Route("/api/project_has_user/usersNotActiveInProject/{projectId}", name="users_not_active_in_project", methods={"GET", "OPTIONS"} )
     */
    public function usersNotActiveInProject(Helpers $helpers, $projectId, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);
        if ($token['check'] && $user->getRoles()[0] == 'ROLE_ADMIN' && $user->getActive() == 1) {          
            $users = $this->getDoctrine()->getManager()->getRepository(ProjectHasUser::class)->usersNotActiveInProject($projectId);
            if ($users) {
                return $helpers->json([
                            'ok' => true,
                            'message' => 'Success!',
                            'data' => $users
                                ], 200);

            }
            return new JsonResponse(['message' => 'Success','data' => null, 'ok' => true], 200);
        }
        return new JsonResponse(['message' => 'Not authorized', 'ok' => false], 404);
    }
    
}
