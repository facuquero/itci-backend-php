<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Role;
use App\Service\Helpers;
use App\Service\jwtAuth;
use App\Entity\UserHasRole;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserHasRoleController extends AbstractController {

    /**
     * @Route("/api/user_has_role", name="add_user_has_role", methods={"POST", "OPTIONS"} )
     */
    public function add(Request $request, Helpers $helpers, $params = [], jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);

        if ($token['check'] && $user->getRoles()[0] = 'ROLE_ADMIN') {
            $parameters = $request->request->all();
            if (!empty($params)) {
                $parameters = $params;
            }
            $user = $this->getDoctrine()->getManager()->getRepository(User::class)->findOneBy(['id' => $parameters['user']]);
            $role = $this->getDoctrine()->getManager()->getRepository(Role::class)->findOneBy(['id' => $parameters['role']]);
            $userHasRole = new UserHasRole();
            $userHasRole->setUser($user);
            $userHasRole->setRole($role);
            $errors = $helpers->validateEntity($userHasRole);
            // check role error
            if (!$errors) {
                $userHasRole = $helpers->saveEntity($userHasRole);
                return new JsonResponse([
                    'ok' => true,
                    'data' => $userHasRole->getId(),
                    'message' => 'Success, userHasRole created!'
                        ], 201);
            }
            return new JsonResponse([
                'ok' => false,
                'message' => 'role exist'
                    ], 403);
        }
        return new JsonResponse(['message' => 'Not authorized', 'ok' => false], 404);
    }

    /**
     * @Route("/api/user_has_role/{id}", name="find_user_has_role_by_id", methods={"GET", "OPTIONS"} )
     */
    public function findById(Request $request, Helpers $helpers, $id) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);

        if ($token['check'] && $user->getRoles()[0] = 'ROLE_ADMIN') {
            $userHasRole = $this->getDoctrine()->getRepository(UserHasRole::class)->find($id);
            if ($userHasRole) {
                return $helpers->json([
                            'ok' => true,
                            'message' => 'Success!',
                            'data' => $userHasRole
                                ], 200);
            }
            return new JsonResponse(['message' => 'The userHasRole to search (' . $id . ') not exists', 'ok' => false], 400);
        }
        return new JsonResponse(['message' => 'Not authorized', 'ok' => false], 404);
    }

}
