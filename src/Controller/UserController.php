<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Role;
use App\Entity\Profile;
use App\Service\Helpers;
use App\Service\jwtAuth;
use App\Entity\UserHasRole;
use Doctrine\Bundle\DoctrineBundle\Controller\ProfilerController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserController extends AbstractController {

    /**
     * @Route("/api/login", name="login", methods={"POST", "OPTIONS"} )
     */
    public function login(Request $request, Helpers $helpers, jwtAuth $jwtAuth) {


        try {
            $parameters = $request->request->all();
            $user = $this->getDoctrine()->getManager()->getRepository(User::class)->findOneBy(['email' => $parameters['email']]);
            if ($user) { // return user
                if (password_verify($parameters['password'], $user->getPassword())) {
                    $token = $jwtAuth->signUp($user);
                    $user->setApiToken($token);
                    $user = $helpers->saveEntity($user);
                    $user->setPassword('encripted');
                    return $helpers->json(array('data' => $user, 'ok' => true), 200);
                }
            }
            return new JsonResponse(['message' => 'You could not be authenticated', 'ok' => false], 403);
        } catch (\Exception $ex) {
            dump($ex);
            die;
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage(), 'ok' => false], 500);
        }
    }

    /**
     * @Route("/api/user", name="add_user", methods={"POST", "OPTIONS"} )
     */
    public function add(Request $request, Helpers $helpers, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);
        if (!$token['check'] || $user->getRoles()[0] != 'ROLE_ADMIN') {

            return new JsonResponse(['message' => 'You are not authorized', 'ok' => false], 403);
        }
        $roleInstance = $this->getDoctrine()->getManager()->getRepository(Role::class)->findOneBy(['id' => 2]);
        $parameters = $request->request->all();
        $aleatoria = random_int(10000000, 99999999);
        $parameters['password'] = password_hash($aleatoria, PASSWORD_DEFAULT);
        $parameters['username'] = $parameters['email'];
        $newUser = $helpers->setParametersToEntity(new User(), $parameters, ['password']);
        $newUser->setPassword($parameters['password']);
        $errors = $helpers->validateEntity($newUser);
        // check user error


        if (!$errors) {
            $newUser = $helpers->saveEntity($newUser);

            $newProfile = new Profile();
            $newProfile->setUser($newUser);
            $errorsProfile = $helpers->validateEntity($newProfile);
            if (!$errorsProfile) {
                $newProfile = $helpers->saveEntity($newProfile);
                $params = [
                    "user" => $newUser,
                    "role" => $roleInstance
                ];
                $response = json_decode($this->forward('App\Controller\UserHasRoleController::add', ['params' => $params
                        ])->getContent(), 1);

                $params = ['mensaje' => '<h1> Bienvenido a ITCI, tu contraseña es: ' . $aleatoria . '</h1>',
                    'to' => $newUser->getEmail(), 'subject' => 'Welcome to ITCI'];
                $response = json_decode($this->forward('App\Controller\EmailSystemController::sendMail', ['params' => $params
                        ])->getContent(), 1);

                return new JsonResponse([
                    'ok' => true,
                    'data' => 'ok',
                    'message' => 'Success, user created!'
                        ], 201);
            }
            return new JsonResponse([
                'ok' => false,
                'message' => 'User exist'
                    ], 403);
        }
        return new JsonResponse([
            'ok' => false,
            'message' => $errors
                ], 403);
    }

    /**
     * @Route("/api/user/{userId}", name="find_user_by_id", methods={"GET", "OPTIONS"} )
     */
    public function findById(Request $request, Helpers $helpers, $userId, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);
        if ($token['check'] && $user->getRoles()[0] == 'ROLE_ADMIN' || $token['check'] && $user->getRoles()[0] == 'ROLE_DEV') {
            $userFound = $this->getDoctrine()->getRepository(User::class)->find($userId);
            if ($user->getRoles()[0] == 'ROLE_DEV' && $userFound) {
                $userLimited['name'] = $userFound->getName() . ' ' . $userFound->getLastname();
                $userLimited['email'] = $userFound->getEmail();
                $userFound = $userLimited;
            }
            if ($userFound) {
                $userFound = !is_array($userFound) ? $userFound->setPassword('encripted') : $userFound;
                return $helpers->json([
                            'ok' => true,
                            'message' => 'Success!',
                            'data' => $userFound
                                ], 200);
            }
            return new JsonResponse(['message' => 'Success', 'ok' => true, 'data' => null], 200);
        }
        return new JsonResponse(['message' => 'You are not authorized', 'ok' => false], 404);
    }

    /**
     * @Route("/api/user", name="find_all_users", methods={"GET", "OPTIONS"})
     */
    public function findAll(Helpers $helpers, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);
        if ($token['check'] && $user->getRoles()[0] != 'ROLE_CLIENT') {
            $users = $this->getDoctrine()->getRepository(User::class)->findAll();
            if ($users) {
                $userList = array();
                foreach ($users as &$userIndividual) {
                    $userLimited['name'] = $userIndividual['name'] . ' ' . $userIndividual['lastname'];
                    $userLimited['email'] = $userIndividual['email'];
                    $userIndividual['password'] = 'encripted';
                    $userList[] = $userLimited;
                }
                $users = $user->getRoles()[0] == 'ROLE_DEV' ? $users = $userList : $users;
                return $helpers->json([
                            'ok' => true,
                            'message' => 'Success!',
                            'data' => $users
                                ], 200);
            }
            return new JsonResponse(['message' => 'Success', 'ok' => true, 'data' => null], 200);
        }
        return new JsonResponse(['message' => 'You are not authorized', 'ok' => false], 404);
    }

    /**
     * @Route("/api/user", name="update_user", methods={"PUT", "OPTIONS"} )
     */
    public function update(Request $request, Helpers $helpers, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);
        if ($token['check']) {
            $parameters = $request->request->all();
            $updateUser = $helpers->setParametersToEntity($user, $parameters, ['id']);
            $errors = $helpers->validateEntity($user);
            if (!$errors) {
                $user = $helpers->saveEntity($user);
                return new JsonResponse(['message' => 'Success, user updated!', 'ok' => true], 200);
            }
            return new JsonResponse(['message' => 'Error validating', 'errors' => $errors, 'ok' => false], 400);
        }
        return new JsonResponse(['message' => 'You are not authorized', 'ok' => false], 404);
    }

    /**
     * @Route("/api/user/{userId}", name="delete_user", methods={"DELETE", "OPTIONS"} )
     */
    public function deleteUser(Helpers $helpers, $userId, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);
        if ($token['check'] && $user->getActive() == 1) {
            $userDelete = $this->getDoctrine()->getRepository(User::class)->find($userId);
            if ($userDelete) {
                if (($user->getRoles()[0] == 'ROLE_ADMIN') || ($user->getId() == $userDelete->getId())) {

                    if ($userDelete->getRoles()[0] != null) {
                        $userHasRoleDelete = $this->getDoctrine()->getRepository(UserHasRole::class)->findOneBy(['user' => $userId]);
                        $helpers->removeEntity($userHasRoleDelete);
                    }

                    $profile = $this->getDoctrine()->getRepository(Profile::class)->findOneBy(['user' => $userDelete->getId()]);

                    if ($profile->getUser() != null) {

                        $helpers->removeEntity($profile);
                    }
                    $helpers->removeEntity($userDelete);
                    return new JsonResponse(['message' => 'User deleted', 'ok' => true], 200);
                }
                return new JsonResponse(['message' => 'User not authorizated to delete this user', 'ok' => false], 400);
            }
            return new JsonResponse(['message' => 'User not found', 'ok' => false], 400);
        }
        return new JsonResponse(['message' => 'You are not authorized', 'ok' => false], 400);
    }

    /**
     * @Route("/api/user/recover_password", name="recover_password", methods={"POST", "OPTIONS"})
     */
    public function recoverPassword(Request $request, Helpers $helpers) {
        $parameters = $request->request->all();
        $email = $parameters['email'];
        $user = $this->getDoctrine()->getRepository(User::class)->findOneByEmail($email);
        if ($user) {
            $aleatoria = random_int(10000000, 99999999);
            $parameters['password'] = password_hash($aleatoria, PASSWORD_DEFAULT);
            $updateUser = $helpers->setParametersToEntity($user, $parameters, ['id', 'password']);
            $updateUser->setPassword($parameters['password']);
            $errors = $helpers->validateEntity($user);
            if (!$errors) {
                $user = $helpers->saveEntity($user);
                $params = ['mensaje' => '<h1> Has recuperado tu contraseña con exito, tu nueva contraseña es: ' . $aleatoria . '</h1>',
                    'to' => $email, 'subject' => 'You new password'];
                $response = json_decode($this->forward('App\Controller\EmailSystemController::sendMail', ['params' => $params
                        ])->getContent(), 1);
                return $helpers->json([
                            'ok' => true,
                            'message' => 'Password actualizada!',
                            'password' => 'sended'
                                ], 201);
            }
            return new JsonResponse(['message' => 'no hay niguno', 'ok' => false], 404);
        }
    }

    /**
     * @Route("/api/change_password", name="change_password", methods={"PUT", "OPTIONS"} )
     */
    public function changePassword(Request $request, Helpers $helpers, jwtAuth $jwtAuth) {
        try {
            $user = $this->getUser();
            $token = $jwtAuth->checkToken($user);
            $parameters = $request->request->all();
            if ($token['check']) {
                $parameters['password'] = password_hash($parameters['password'], PASSWORD_DEFAULT);
                $user->setPassword($parameters['password']);
                $helpers->validateEntity($user);
                $helpers->saveEntity($user);
                $params = ['mensaje' => '<h1> Se ha cambiado la contraseña de tu cuenta en ITCI, si no haz sido tu ponte en contacto con el servicio tecnico </h1>',
                    'to' => $user->getEmail(), 'subject' => 'Changed password'];
                $response = json_decode($this->forward('App\Controller\EmailSystemController::sendMail', ['params' => $params
                        ])->getContent(), 1);
                return new JsonResponse([
                    'ok' => true,
                    'data' => 'ok'
                        ], 200);
            }
            return new JsonResponse(['message' => $token['message'], 'ok' => false], 403);
        } catch (\Exception $ex) { //dump($ex);die;
            return new JsonResponse(['message' => 'Operation failed. ' . $ex->getMessage(), 'ok' => false], 500);
        }
    }

}
