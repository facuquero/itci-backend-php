<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Project;
use App\Service\Helpers;
use App\Service\jwtAuth;
use App\Entity\UserHasRole;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProjectController extends AbstractController {

    /**
     * @Route("/api/project", name="add_project", methods={"POST", "OPTIONS"} )
     */
    public function add(Request $request, Helpers $helpers, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);

        if ($token['check'] && $user->getRoles()[0] = 'ROLE_ADMIN') {
            $parameters = $request->request->all();
            $userClient = $this->getDoctrine()->getRepository(User::class)->find($parameters['client']);
            if ($userClient) {
                $newProject = $helpers->setParametersToEntity(new Project(), $parameters);
                $newProject->setClient($userClient);

                $newProject = $helpers->saveEntity($newProject);
                return new JsonResponse([
                    'ok' => true,
                    'data' => $newProject->getId(),
                    'message' => 'Success, project created!'
                        ], 201);
            }
            return new JsonResponse([
                'message' => 'Client not found',
                'ok' => false
                    ], 403);
        }
        return new JsonResponse([
            'message' => 'You are not authorized',
            'ok' => false
                ], 403);
    }

    /**
     * @Route("/api/project/{project_id}", name="update_project", methods={"PUT", "OPTIONS"} )
     */
    public function update(Request $request, $project_id, Helpers $helpers, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);
        if ($token['check'] && $user->getRoles()[0] = 'ROLE_ADMIN') {
            $projectUpdate = $this->getDoctrine()->getRepository(Project::class)->find($project_id);
            $parameters = $request->request->all();
            $projectUpdate = $helpers->setParametersToEntity($projectUpdate, $parameters, ['id', 'client', 'finalized', 'status']);
            $projectUpdate = $helpers->saveEntity($projectUpdate);
            return new JsonResponse(['message' => 'Success, project updated!', 'ok' => true], 201);
        }
        return new JsonResponse(['message' => 'You are not authorized', 'ok' => false], 400);
    }

    /**
     * @Route("/api/project/{project_id}", name="find_project_by_id", methods={"GET", "OPTIONS"} )
     */
    public function findById(Request $request, Helpers $helpers, $project_id, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);
        if ($token['check'] && $user->getRoles()[0] == 'ROLE_ADMIN' || $token['check'] && $user->getActive() == 1 && $user->getRoles()[0] == 'ROLE_DEV') {
            $projectFound = $this->getDoctrine()->getRepository(Project::class)->find($project_id);
            if ($projectFound) {
                return $helpers->json([
                            'ok' => true,
                            'message' => 'Success!',
                            'data' => $projectFound
                                ], 200);
            }
            return new JsonResponse(['message' => 'Project not found', 'ok' => false, 'data' => null], 400);
        }
        return new JsonResponse(['message' => 'You are not authorized', 'ok' => false], 404);
    }

    /**
     * @Route("/api/project", name="find_all_projects", methods={"GET", "OPTIONS"} )
     */
    public function findAll(Request $request, Helpers $helpers, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);
        if ($token['check'] && $user->getRoles()[0] == 'ROLE_ADMIN' || $token['check'] && $user->getActive() == 1 && $user->getRoles()[0] == 'ROLE_DEV') {
            $projects = $this->getDoctrine()->getRepository(Project::class)->findAll();
            if ($projects) {
                return $helpers->json([
                            'ok' => true,
                            'message' => 'Success!',
                            'data' => $projects
                                ], 200);
            }
            return new JsonResponse(['message' => 'Project not found', 'ok' => false, 'data' => null], 400);
        }
        return new JsonResponse(['message' => 'You are not authorized', 'ok' => false], 404);
    }

    /**
     * @Route("/api/project/{projectId}", name="delete_project", methods={"DELETE", "OPTIONS"} )
     */
    public function deleteProject(Helpers $helpers, $projectId, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);       
        if ($token['check'] && $user->getActive() == 1 && $user->getRoles()[0] == 'ROLE_ADMIN') {
            $projectDelete = $this->getDoctrine()->getRepository(Project::class)->find($projectId);
            if ($projectDelete) {
                $helpers->removeEntity($projectDelete);
                return new JsonResponse(['message' => 'Project deleted', 'ok' => true], 200);
            }
            return new JsonResponse(['message' => 'Project not found', 'ok' => false], 400);
        }
        return new JsonResponse(['message' => 'User not authorizated to delete Projects', 'ok' => false], 400);
    }

}
