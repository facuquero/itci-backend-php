<?php

namespace App\Controller;

use App\Entity\Role;
use App\Service\Helpers;
use App\Service\jwtAuth;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RoleController extends AbstractController {

    /**
     * @Route("/api/role", name="add_role", methods={"POST", "OPTIONS"} )
     */
    public function add(Request $request, Helpers $helpers, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);
        if ($token['check'] && $user->getRoles()[0] == 'ROLE_ADMIN') {
            $parameters = $request->request->all();
            $newRole = $helpers->setParametersToEntity(new Role(), $parameters);
            $errors = $helpers->validateEntity($newRole);
            // check role error
            if (!$errors) {
                $newRole = $helpers->saveEntity($newRole);
                return new JsonResponse([
                    'ok' => true,
                    'data' => $newRole->getId(),
                    'message' => 'Success, role created!'
                        ], 201);
            }
            return new JsonResponse([
                'ok' => false,
                'message' => 'role exist'
                    ], 403);
        }
        return new JsonResponse(['message' => 'Not authorized', 'ok' => false], 404);
    }

    /**
     * @Route("/api/role", name="find_all_roles", methods={"GET", "OPTIONS"})
     */
    public function findAll(Helpers $helpers, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);
        if ($token['check'] && $user->getRoles()[0] == 'ROLE_ADMIN') {
            $roles = $this->getDoctrine()->getRepository(Role::class)->findAll();
            if ($roles) {
                return $helpers->json([
                            'ok' => true,
                            'message' => 'Success!',
                            'data' => $roles
                                ], 200);
            }
            return new JsonResponse([
                'ok' => false,
                'message' => 'Empty'
                    ], 404);
        }
        return new JsonResponse(['message' => 'Not authorized', 'ok' => false], 404);
    }

    /**
     * @Route("/api/role/{roleId}", name="find_role_by_id", methods={"GET", "OPTIONS"} )
     */
    public function findById(Request $request, Helpers $helpers, $roleId, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);
        if ($token['check'] && $user->getRoles()[0] == 'ROLE_ADMIN') {
            $roleFound = $this->getDoctrine()->getRepository(Role::class)->find($roleId);
            if ($roleFound) {
                return $helpers->json([
                            'ok' => true,
                            'message' => 'Success!',
                            'data' => $roleFound
                                ], 200);
            }
            return new JsonResponse(['message' => 'The role to search (' . $roleId . ') not exists', 'ok' => false], 400);
        }
        return new JsonResponse(['message' => 'Not authorized', 'ok' => false], 404);
    }

    /**
     * @Route("/api/role/{roleId}", name="delete_role", methods={"DELETE", "OPTIONS"} )
     */
    public function deleteRole(Helpers $helpers, $roleId, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);
        if ($token['check'] && $user->getRoles()[0] == 'ROLE_ADMIN') {
            $roleDelete = $this->getDoctrine()->getRepository(Role::class)->find($roleId);
            if ($roleDelete) {
                $helpers->removeEntity($roleDelete);
                return new JsonResponse(['message' => 'Role deleted', 'ok' => true], 200);
            }
            return new JsonResponse(['message' => 'Role not found', 'ok' => false], 400);
        }
        return new JsonResponse(['message' => 'Not authorized', 'ok' => false], 404);
    }

}
