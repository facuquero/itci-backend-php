<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EmailSystemController extends AbstractController {

    /**
     * @Route("/api/emailsystem", name="send_email", methods={"PUT", "OPTIONS"} )
     */
    public function sendMail(Request $request, \Swift_Mailer $mailer, $params=[]) {
       # $pathStatics = $this->getParameter('host_statics');
        $parameters = $request->request->all();
        if (!empty($params)){
            $parameters = $params;
            #$params['pathStatics'] = $pathStatics;
        }
        $mensaje = $parameters['mensaje'];
        $message = (new \Swift_Message())
                ->setSubject($parameters['subject'])
                ->setFrom(array('fedeferreprueba@gmail.com' => 'ITCI'))
                ->setTo($parameters['to'])
                ->setBody($mensaje,'text/html');
                
     /*   switch ($parameters['template']) {
            case 'welcome':
                $params['pathStatics'] = $pathStatics;
                $message = $this->setParamsToMessage('welcome', $params, $message, $parameters['lenguage']);
                break;
            case 'rejected':
                $userSend = $this->getDoctrine()->getManager()->getRepository(User::class)->findOneBy(['email' => $parameters['to']]);
                $name = $userSend->getName();
                $params = array('name' => $userSend->getName(), 'clubName' => $p, 'pathStatics' => $pathStatics);
                $message = $this->setParamsToMessage('rejected', $params, $message, $parameters['lenguage']);
                break;
            case 'userautorizate':
                /* Trigger in AdduserPend Controller*/
           /*     $message = $this->setParamsToMessage('userautorizate', $params, $message, $parameters['lenguage']);
                break;
            case 'changepassword':
                #trigger in forgotMyPassword UserController
                $message = $this->setParamsToMessage('changepassword', $params, $message, $parameters['lenguage']);
                break;
            case 'changedpassword':
                $userSend = $this->getDoctrine()->getManager()->getRepository(User::class)->findOneBy(['email' => $parameters['to']]);
                $params = array('name' => $userSend->getName());
                $message = $this->setParamsToMessage('changedpassword', $params, $message, $parameters['lenguage']);
                break;
            case 'userdeleted':
                break;
            case 'validateuser':
                #trigger en AddUserPend UserController
                $message = $this->setParamsToMessage('validateuser', $params, $message, $parameters['lenguage']);
                break;
            default:
                break;
        }*/
        
        $result = $mailer->send($message);
        if ($result == 1) {
            return new JsonResponse(array('error' => 0, 'code' => 200, 'response' => 'sended'));
        }
        return new JsonResponse(['message' => 'Operation failed. ', 'ok' => false], 500);
    }

    private function setParamsToMessage($template, $params = [], $message, $lenguage) {
        $message->setBody(
                $this->renderView(
                        'email/'.$template . '-' . $lenguage . '.html.twig'
                        , $params), 'text/html'
        );
        return $message;
    }

}
