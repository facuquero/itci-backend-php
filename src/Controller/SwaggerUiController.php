<?php

namespace App\Controller;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SwaggerUiController extends AbstractController {

    /**
     * @Route("/api/doc", name="swagger_ui")
     */
    public function index() {

    	$yamlFile = __DIR__ . '/../../templates/swagger_ui/itci-swagger.yaml';

        $viewData = [
            'spec' =>json_encode(Yaml::parseFile($yamlFile)),
        ];

        return $this->render('swagger_ui/swagger.twig', $viewData);
    }
}
