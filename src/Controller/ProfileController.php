<?php

namespace App\Controller;

use App\Controller\UserController;
use App\Entity\Profile;
use App\Entity\UserHasRole;
use App\Service\Helpers;
use App\Service\jwtAuth;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController {

    /**
     * @Route("/api/profile", name="update_profile", methods={"PUT", "OPTIONS"} )
     */
    public function updateProfile(Request $request, Helpers $helpers, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);
        if ($token['check']) {
            $parameters = $request->request->all();
            $updateUser = $helpers->setParametersToEntity(new Profile(), $parameters, ['id']);
            $errors = $helpers->validateEntity($updateUser);
            if (!$errors) {
                $profile = $helpers->saveEntity($updateUser);
                return new JsonResponse(['message' => 'Success, user updated!', 'ok' => true], 200);
            }
            return new JsonResponse(['message' => 'Error validating', 'errors' => $errors, 'ok' => false], 400);
        }
        return new JsonResponse(['message' => 'no estas autorizado', 'ok' => false], 404);
    }

    /**
     * @Route("/api/profile/{profileId}", name="find_profile_by_id", methods={"GET", "OPTIONS"} )
     */
    public function findById(Request $request, Helpers $helpers, $profileId, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);

        if ($token['check'] && $user->getRoles()[0] == 'ROLE_ADMIN' || $token['check'] && $user->getRoles()[0] == 'ROLE_DEV') {
            $profileFound = $this->getDoctrine()->getRepository(Profile::class)->find($profileId);

            if ($user->getRoles()[0] == 'ROLE_DEV' && $profileFound) {
                $profileFound->setPhone("null");
            }
            if ($profileFound) {
                # $profileFound = !is_array($profileFound) ? $profileFound->setPassword('encripted') : $profileFound;
                return $helpers->json([
                            'ok' => true,
                            'message' => 'Success!',
                            'data' => $profileFound
                                ], 200);
            }
            return new JsonResponse(['message' => 'Users not found', 'ok' => false, 'data' => null], 404);
        }
        return new JsonResponse(['message' => 'You are not authorized', 'ok' => false], 404);
    }
///findbyuserId
        /**
     * @Route("/api/profile/findByUserId/{userId}", name="find_profile_by_user_id", methods={"GET", "OPTIONS"} )
     */
    public function findByUserId(Request $request, Helpers $helpers, $userId, jwtAuth $jwtAuth) {
        $user = $this->getUser();
        $token = $jwtAuth->checkToken($user);

        if ($token['check'] && $user->getRoles()[0] == 'ROLE_ADMIN' || $token['check'] && $user->getRoles()[0] == 'ROLE_DEV') {
            $profileFound = $this->getDoctrine()->getRepository(Profile::class)->findOneby(['user' => $userId]);

            if ($user->getRoles()[0] == 'ROLE_DEV' && $profileFound) {
                $profileFound->setPhone("null");
                
            }
            if ($profileFound) {
                #$profileFound = !is_array($profileFound) ? $profileFound->setPassword('encripted') : $profileFound;
                return $helpers->json([
                            'ok' => true,
                            'message' => 'Success!',
                            'data' => $profileFound
                                ], 200);
            }
            return new JsonResponse(['message' => 'Success', 'ok' => true, 'data' => null], 200);
        }
        return new JsonResponse(['message' => 'You are not authorized', 'ok' => false], 404);
    }
}///documentar: si es post tiene un requestbody
