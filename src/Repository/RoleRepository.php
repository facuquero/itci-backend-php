<?php

namespace App\Repository;

use App\Entity\Role;
use App\Service\Helpers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Role|null find($id, $lockMode = null, $lockVersion = null)
 * @method Role|null findOneBy(array $criteria, array $orderBy = null)
 * @method Role[]    findAll()
 * @method Role[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoleRepository extends ServiceEntityRepository {

    public $helpers;

    public function __construct(ManagerRegistry $registry,Helpers $helpers) {
        parent::__construct($registry, Role::class);
        $this->helpers = $helpers;
    }

    public function getRoleByUserId($userId) {
        $sql = 'SELECT r.name FROM role r INNER JOIN user_has_role uhs ON r.id = uhs.role_id WHERE uhs.user_id = ' . $userId;
        return $this->helpers->aplicateConn($sql);
    }

}
