<?php

namespace App\Repository;

use App\Entity\User;
use App\Service\Helpers;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository {

    public $helpers;

    public function __construct(ManagerRegistry $registry, Helpers $helpers) {
        parent::__construct($registry, User::class);
        $this->helpers = $helpers;
    }

    public function findAll() {
        $sql = 'SELECT * FROM user';
        return $this->helpers->aplicateConn($sql);
    }

    public function deleteUser($userId) {
        try {
            $sql = 'DELETE FROM user WHERE user.id = ' . $userId;
            $this->helpers->aplicateConn($sql, [], true);
            return ['error' => false];
        } catch (\Throwable $e) {
            return ['error' => true, 'message' => $e->getMessage()];
        }
    }



}
