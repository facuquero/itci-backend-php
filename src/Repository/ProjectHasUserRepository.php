<?php

namespace App\Repository;

use App\Service\Helpers;
use App\Entity\ProjectHasUser;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method ProjectHasUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProjectHasUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProjectHasUser[]    findAll()
 * @method ProjectHasUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectHasUserRepository extends ServiceEntityRepository {

    public $helpers;

    public function __construct(ManagerRegistry $registry,Helpers $helpers) {
        parent::__construct($registry, ProjectHasUser::class);
        $this->helpers = $helpers;
    }

    public function usersNotActiveInProject($projectId) {

            $sql = 'SELECT * FROM project_has_user where project_id = ' . $projectId . ' and active= 0';
            $respuesta = $this->helpers->aplicateConn($sql);
            return $respuesta;
   
    }

    // /**
    //  * @return ProjectHasUser[] Returns an array of ProjectHasUser objects
    //  */
    /*
      public function findByExampleField($value)
      {
      return $this->createQueryBuilder('p')
      ->andWhere('p.exampleField = :val')
      ->setParameter('val', $value)
      ->orderBy('p.id', 'ASC')
      ->setMaxResults(10)
      ->getQuery()
      ->getResult()
      ;
      }
     */

    /*
      public function findOneBySomeField($value): ?ProjectHasUser
      {
      return $this->createQueryBuilder('p')
      ->andWhere('p.exampleField = :val')
      ->setParameter('val', $value)
      ->getQuery()
      ->getOneOrNullResult()
      ;
      }
     */
}
