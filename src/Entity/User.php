<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity("email")
 */
class User implements UserInterface {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dni;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $apiToken;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="integer")
     */
    private $active = 1;

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(?string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getLastname(): ?string {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(string $email): self {
        $this->email = $email;

        return $this;
    }

    public function getDni(): ?string {
        return $this->dni;
    }

    public function setDni(?string $dni): self {
        $this->dni = $dni;

        return $this;
    }

    public function getPassword(): ?string {
        return $this->password;
    }

    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }

    public function getApiToken(): ?string {
        return $this->apiToken;
    }

    public function setApiToken(string $apiToken): self {
        $this->apiToken = $apiToken;
        return $this;
    }

    public function getUsername(): ?string {
        return $this->username;
    }

    public function setUsername(?string $username): self {
        $this->username = $username;
        return $this;
    }

    public function eraseCredentials() {
        
    }

    public function getSalt() {
        
    }

    public function getRoles(): array {
        $em = $GLOBALS['kernel']->getContainer()->get('doctrine')->getManager();
        $roles = $em->getRepository(UserHasRole::class)->findOneByUser($this->id);
        if ($roles == null) {
            $rolesClean[] = 'caca';
            return array_unique($rolesClean);
        }
        $rolesClean[] = $roles->getRole()->getName();
        return array_unique($rolesClean);
    }

    public function getActive(): ?int {
        return $this->active;
    }

    public function setActive(int $active): self {
        $this->active = $active;

        return $this;
    }

}
