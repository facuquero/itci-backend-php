<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 */
class Project {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class , fetch="LAZY")
     */
    private $Client;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $dashboard;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status=0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $finalized=0;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $startDate;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $endDate;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $applicationEndDate;

    public function __construct() {
        $this->project = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getClient(): ?User {
        return $this->Client;
    }

    public function setClient(User $client): self {
        $this->Client = $client;

        return $this;
    }

    public function getDashboard(): ?string {
        return $this->dashboard;
    }

    public function setDashboard(?string $dashboard): self {
        $this->dashboard = $dashboard;

        return $this;
    }

    public function getStatus(): ?int {
        return $this->status;
    }

    public function setStatus(?int $status): self {
        $this->status = $status;

        return $this;
    }

    public function getFinalized(): ?int {
        return $this->finalized;
    }

    public function setFinalized(?int $finalized): self {
        $this->finalized = $finalized;

        return $this;
    }

    public function getStartDate(): ?string {
        return $this->startDate;
    }

    public function setStartDate(?string $startDate): self {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?string {
        return $this->endDate;
    }

    public function setEndDate(?string $endDate): self {
        $this->endDate = $endDate;

        return $this;
    }

    public function getApplicationEndDate(): ?string {
        return $this->applicationEndDate;
    }

    public function setApplicationEndDate(?string $applicationEndDate): self {
        $this->applicationEndDate = $applicationEndDate;

        return $this;
    }

}
