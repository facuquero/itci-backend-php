<?php

namespace App\Entity;

use App\Repository\ProjectHasUserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=ProjectHasUserRepository::class)
 * @UniqueEntity(fields={"project", "user"}, message="this value (%s) already exists in the (%s).")
 */
class ProjectHasUser {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class)
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $user;

    /**
     * @ORM\Column(type="integer")
     */
    private $active = 0;

    public function getId(): ?int {
        return $this->id;
    }

    public function getProject(): ?Project {
        return $this->project;
    }

    public function setProject(?Project $project): self {
        $this->project = $project;

        return $this;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getActive(): ?int {
        return $this->active;
    }

    public function setActive(int $active): self {
        $this->active = $active;

        return $this;
    }

}
