<?php

namespace App\Service;

use Firebase\JWT\JWT;

class jwtAuth {

    public $manager;
    public $key;
    private static $aud = null;

    public function __construct($manager) {
        $this->manager = $manager;
        $this->key = 'clave secreta';
    }

    public function signUp($user) {

        $token = array(
            "id" => $user->getId(),
            'aud' => self::Aud(),
            "iat" => time(),
            "exp" => time() + 86400
        );
        $jwtEncode = JWT::encode($token, $this->key, 'HS256');
        $jwtDecode = JWT::decode($jwtEncode, $this->key, array('HS256'));

        return $jwtEncode;
    }

    public function checkToken($user) {

        $auth = array('check' => false, 'message' => 'You are not authorized to perform this action.');

        if ($user) {
            $jwt = $user->getApiToken();
            try {
                $decode = JWT::decode($jwt, $this->key, array('HS256'));
            } catch (\InvalidArgumentException $e) {
                $auth = array('check' => false, 'message' => 'Invalid argument!');
            } catch (\DomainException $e) {
                $auth = array('check' => false, 'message' => 'Login failed!');
            } catch (\UnexpectedValueException $e) {
                $auth = array('check' => false, 'message' => 'Login failed! or expired!');
            }
            if (isset($decode) && is_object($decode) && isset($decode->id)) {

                if ($decode->aud !== self::Aud()) {
                    $auth = array('check' => false, 'message' => 'Invalid user logged in.');
                } else {
                    $auth = array(
                        'check' => true,
                        'id' => $decode->id,
                        'aud' => self::Aud(),
                        "iat" => $decode->iat,
                        "exp" => $decode->exp
                    );
                }
            }
        }

        return $auth;
    }

    private static function Aud() {

        $aud = '';
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $aud = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $aud = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $aud = $_SERVER['REMOTE_ADDR'];
        }
        $aud .= @$_SERVER['HTTP_USER_AGENT'];
        $aud .= gethostname();
        return sha1($aud);
    }

}
