<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210419220644 extends AbstractMigration {

    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE profile (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, biography VARCHAR(1000) DEFAULT NULL, photo VARCHAR(255) DEFAULT NULL, area VARCHAR(255) NOT NULL, lenguage VARCHAR(255) DEFAULT NULL, technology VARCHAR(500) NOT NULL, social_media VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8157AA0FA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project (id INT AUTO_INCREMENT NOT NULL, client INT DEFAULT NULL, dashboard LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', status INT DEFAULT NULL, finalized TINYINT(1) DEFAULT NULL, start_date DATETIME DEFAULT NULL, end_date DATETIME DEFAULT NULL, application_end_date DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project_has_user (id INT AUTO_INCREMENT NOT NULL, project_id INT DEFAULT NULL, user_id INT DEFAULT NULL, INDEX IDX_ED8186CD166D1F9C (project_id), INDEX IDX_ED8186CDA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project_has_users_pending (id INT AUTO_INCREMENT NOT NULL, project_id INT DEFAULT NULL, user_pending_id INT DEFAULT NULL, INDEX IDX_B2FF2E4B166D1F9C (project_id), INDEX IDX_B2FF2E4B28C071B0 (user_pending_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, lastname VARCHAR(255) DEFAULT NULL, email VARCHAR(255) NOT NULL, dni VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, api_token VARCHAR(255) DEFAULT NULL, username VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_has_role (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, role_id INT NOT NULL, active INT NOT NULL, UNIQUE INDEX UNIQ_EAB8B535A76ED395 (user_id), INDEX IDX_EAB8B535D60322AC (role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE profile ADD CONSTRAINT FK_8157AA0FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE project_has_user ADD CONSTRAINT FK_ED8186CD166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE project_has_user ADD CONSTRAINT FK_ED8186CDA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE project_has_users_pending ADD CONSTRAINT FK_B2FF2E4B166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE project_has_users_pending ADD CONSTRAINT FK_B2FF2E4B28C071B0 FOREIGN KEY (user_pending_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_has_role ADD CONSTRAINT FK_EAB8B535A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_has_role ADD CONSTRAINT FK_EAB8B535D60322AC FOREIGN KEY (role_id) REFERENCES role (id)');
        $p = password_hash('hola', PASSWORD_DEFAULT);
        $this->addSql('INSERT INTO user(id,name,lastname,dni,email,username,password, api_token) VALUES (1' . ',' . "'super'" . ',' . "'user'" . ',' . "'40107525'" . ',' . "'info@hi-itci.com'" . ',' . "'info@hi-itci.com'" . ',' . "'" . $p . "'" . ', null)');
        $this->addSql('INSERT INTO user(id,name,lastname,dni,email,username,password, api_token) VALUES (2' . ',' . "'dev'" . ',' . "'user'" . ',' . "'40107525'" . ',' . "'rrhh@hi-itci.com'" . ',' . "'rrhh@hi-itci.com'" . ',' . "'" . $p . "'" . ', null)');
        $this->addSql('INSERT INTO role(id,name) VALUES (1' . ',' . "'ROLE_ADMIN'" . ')');
        $this->addSql('INSERT INTO role(id,name) VALUES (2' . ',' . "'ROLE_DEV'" . ')');
        $this->addSql('INSERT INTO user_has_role(id,user_id, role_id,active) VALUES (1,1,1,1)');
        $this->addSql('INSERT INTO user_has_role(id,user_id, role_id,active) VALUES (2,2,2,1)');
    }

    public function down(Schema $schema): void {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DELETE FROM user WHERE id = 1');
        $this->addSql('DELETE FROM user WHERE id = 2');
        $this->addSql('ALTER TABLE project_has_user DROP FOREIGN KEY FK_ED8186CD166D1F9C');
        $this->addSql('ALTER TABLE project_has_users_pending DROP FOREIGN KEY FK_B2FF2E4B166D1F9C');
        $this->addSql('ALTER TABLE user_has_role DROP FOREIGN KEY FK_EAB8B535D60322AC');
        $this->addSql('ALTER TABLE profile DROP FOREIGN KEY FK_8157AA0FA76ED395');
        $this->addSql('ALTER TABLE project_has_user DROP FOREIGN KEY FK_ED8186CDA76ED395');
        $this->addSql('ALTER TABLE project_has_users_pending DROP FOREIGN KEY FK_B2FF2E4B28C071B0');
        $this->addSql('ALTER TABLE user_has_role DROP FOREIGN KEY FK_EAB8B535A76ED395');
        $this->addSql('DROP TABLE profile');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE project_has_user');
        $this->addSql('DROP TABLE project_has_users_pending');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_has_role');
    }

}
