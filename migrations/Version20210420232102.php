<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210420232102 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE project_has_users_pending');
        $this->addSql('ALTER TABLE project_has_user ADD active INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE project_has_users_pending (id INT AUTO_INCREMENT NOT NULL, project_id INT DEFAULT NULL, user_pending_id INT DEFAULT NULL, INDEX IDX_B2FF2E4B166D1F9C (project_id), INDEX IDX_B2FF2E4B28C071B0 (user_pending_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE project_has_users_pending ADD CONSTRAINT FK_B2FF2E4B166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE project_has_users_pending ADD CONSTRAINT FK_B2FF2E4B28C071B0 FOREIGN KEY (user_pending_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE project_has_user DROP active');
    }
}
