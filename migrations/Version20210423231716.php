<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210423231716 extends AbstractMigration {

    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        $this->addSql('INSERT INTO profile(user_id) VALUES (1)');
        $this->addSql('INSERT INTO profile(user_id) VALUES (2)');
    }

    public function down(Schema $schema): void {
        $this->addSql('DELETE FROM profile WHERE id = 1');
        $this->addSql('DELETE FROM profile WHERE id = 2');
    }

}
